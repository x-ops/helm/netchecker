# netchecker

![Version: 0.1.5](https://img.shields.io/badge/Version-0.1.5-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.2.2](https://img.shields.io/badge/AppVersion-1.2.2-informational?style=flat-square)

Network checker is checking of connectivity between the cluster's nodes.

**Homepage:** <https://gitlab.com/x-ops/helm/netchecker>

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Anton Kulikov |  | <https://gitlab.com/4ops> |

## Source Code

* <https://gitlab.com/x-ops/helm/netchecker>
* <https://github.com/Mirantis/k8s-netchecker-server>
* <https://github.com/Mirantis/k8s-netchecker-agent>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| agent.image.pullPolicy | string | `"IfNotPresent"` |  |
| agent.image.repository | string | `"docker.io/mirantis/k8s-netchecker-agent"` |  |
| agent.image.tag | string | `""` |  |
| agent.logLevel | int | `5` |  |
| agent.nodeSelector."kubernetes.io/os" | string | `"linux"` |  |
| agent.podAnnotations | object | `{}` |  |
| agent.podSecurityContext | object | `{}` |  |
| agent.priorityClassName | string | `"system-node-critical"` |  |
| agent.probeUrls[0] | string | `"http://api.ipify.org/"` |  |
| agent.reportInterval | int | `15` |  |
| agent.resources.limits.cpu | string | `"30m"` |  |
| agent.resources.limits.memory | string | `"64M"` |  |
| agent.resources.requests.cpu | string | `"30m"` |  |
| agent.resources.requests.memory | string | `"64M"` |  |
| agent.securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| agent.securityContext.readOnlyRootFilesystem | bool | `true` |  |
| agent.securityContext.runAsGroup | int | `1000` |  |
| agent.securityContext.runAsNonRoot | bool | `true` |  |
| agent.securityContext.runAsUser | int | `1000` |  |
| agent.tolerations[0].effect | string | `"NoSchedule"` |  |
| agent.tolerations[0].operator | string | `"Exists"` |  |
| fullnameOverride | string | `""` |  |
| grafanaDashboardConfigMap.create | bool | `false` |  |
| grafanaDashboardConfigMap.labels.grafana_dashboard | string | `"1"` |  |
| grafanaDashboardConfigMap.name | string | `""` |  |
| grafanaDashboardConfigMap.namespace | string | `""` |  |
| imagePullSecrets | list | `[]` |  |
| nameOverride | string | `""` |  |
| podAnnotations | object | `{}` |  |
| psp.create | bool | `false` |  |
| psp.enabled | bool | `false` |  |
| psp.name | string | `""` |  |
| server.affinity | object | `{}` |  |
| server.containerPort | int | `8081` |  |
| server.createRole | bool | `true` |  |
| server.etcd.image.pullPolicy | string | `"IfNotPresent"` |  |
| server.etcd.image.repository | string | `"quay.io/coreos/etcd"` |  |
| server.etcd.image.tag | string | `"v3.4.17"` |  |
| server.etcd.resources.limits.cpu | string | `"300m"` |  |
| server.etcd.resources.limits.memory | string | `"512Mi"` |  |
| server.etcd.resources.requests.cpu | string | `"100m"` |  |
| server.etcd.resources.requests.memory | string | `"256Mi"` |  |
| server.etcd.securityContext.allowPrivilegeEscalation | bool | `false` |  |
| server.etcd.securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| server.etcd.securityContext.runAsGroup | int | `1000` |  |
| server.etcd.securityContext.runAsNonRoot | bool | `true` |  |
| server.etcd.securityContext.runAsUser | int | `1000` |  |
| server.etcdEndpoints[0] | string | `"http://127.0.0.1:2379"` |  |
| server.image.pullPolicy | string | `"IfNotPresent"` |  |
| server.image.repository | string | `"docker.io/mirantis/k8s-netchecker-server"` |  |
| server.image.tag | string | `""` |  |
| server.logLevel | int | `5` |  |
| server.minReadySeconds | int | `0` |  |
| server.nodeSelector."kubernetes.io/os" | string | `"linux"` |  |
| server.podAnnotations."kubectl.kubernetes.io/default-container" | string | `"server"` |  |
| server.podSecurityContext | object | `{}` |  |
| server.priorityClassName | string | `"system-node-critical"` |  |
| server.replicaCount | int | `1` |  |
| server.resources.limits.cpu | string | `"300m"` |  |
| server.resources.limits.memory | string | `"128Mi"` |  |
| server.resources.requests.cpu | string | `"100m"` |  |
| server.resources.requests.memory | string | `"32Mi"` |  |
| server.revisionHistoryLimit | int | `10` |  |
| server.securityContext.allowPrivilegeEscalation | bool | `false` |  |
| server.securityContext.capabilities.drop[0] | string | `"ALL"` |  |
| server.securityContext.runAsGroup | int | `1000` |  |
| server.securityContext.runAsNonRoot | bool | `true` |  |
| server.securityContext.runAsUser | int | `1000` |  |
| server.service.port | int | `80` |  |
| server.service.type | string | `"ClusterIP"` |  |
| server.strategy.rollingUpdate.maxSurge | int | `1` |  |
| server.strategy.rollingUpdate.maxUnavailable | int | `0` |  |
| server.strategy.type | string | `"Recreate"` |  |
| server.terminationGracePeriodSeconds | int | `30` |  |
| server.tolerations[0].effect | string | `"NoSchedule"` |  |
| server.tolerations[0].operator | string | `"Exists"` |  |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `false` |  |
| serviceAccount.name | string | `""` |  |
| serviceMonitor.create | bool | `false` |  |
| serviceMonitor.jobLabel | string | `"app.kubernetes.io/instance"` |  |
| serviceMonitor.name | string | `""` |  |
| serviceMonitor.namespace | string | `""` |  |
| serviceMonitor.scrapeInterval | string | `"30s"` |  |
| serviceMonitor.scrapeTimeout | string | `"15s"` |  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
