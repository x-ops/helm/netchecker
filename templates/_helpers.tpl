{{/*
Expand the name of the chart.
*/}}
{{- define "netchecker.name" -}}
{{-   default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "netchecker.fullname" -}}
{{-   if .Values.fullnameOverride -}}
{{-     .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{-   else -}}
{{-     $name := default .Chart.Name .Values.nameOverride -}}
{{-     if contains $name .Release.Name -}}
{{-       .Release.Name | trunc 63 | trimSuffix "-" -}}
{{-     else -}}
{{-       printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{-     end -}}
{{-   end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "netchecker.chart" -}}
{{-   printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "netchecker.labels" -}}
{{-  include "netchecker.chart" . | quote | printf "helm.sh/chart: %s\n" -}}
{{-  include "netchecker.selectorLabels" . | printf "%s\n" -}}
{{-  .Chart.AppVersion | quote | printf "app.kubernetes.io/version: %s\n" -}}
{{-  .Release.Service | quote | printf "app.kubernetes.io/managed-by: %s" -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "netchecker.selectorLabels" -}}
{{-  include "netchecker.name" . | quote | printf "app.kubernetes.io/name: %s\n" -}}
{{-  .Release.Name | quote | printf "app.kubernetes.io/instance: %s" -}}
{{- end -}}

{{/*
Expand the name of the server.
*/}}
{{- define "netchecker.serverName" -}}
{{-   include "netchecker.fullname" . | printf "%s-server" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Expand the name of the agent.
*/}}
{{- define "netchecker.agentName" -}}
{{-   include "netchecker.fullname" . | printf "%s-agent" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Expand the name of the agent with host network.
*/}}
{{- define "netchecker.hostnetAgentName" -}}
{{-   include "netchecker.fullname" . | printf "%s-hostnet-agent" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Server selector labels
*/}}
{{- define "netchecker.serverSelectorLabels" -}}
{{-  include "netchecker.selectorLabels" . | printf "%s\n" -}}
{{-  "server" | quote | printf "app.kubernetes.io/component: %s\n" -}}
{{-  .Release.Name | quote | printf "app.kubernetes.io/part-of: %s" -}}
{{- end -}}

{{/*
Server labels
*/}}
{{- define "netchecker.serverLabels" -}}
{{-  include "netchecker.chart" . | quote | printf "helm.sh/chart: %s\n" -}}
{{-  include "netchecker.serverSelectorLabels" . | printf "%s\n" -}}
{{-  .Chart.AppVersion | quote | printf "app.kubernetes.io/version: %s\n" -}}
{{-  .Release.Service | quote | printf "app.kubernetes.io/managed-by: %s" -}}
{{- end -}}

{{/*
Agent selector labels
*/}}
{{- define "netchecker.agentSelectorLabels" -}}
{{-  include "netchecker.selectorLabels" . | printf "%s\n" -}}
{{-  "agent" | quote | printf "app.kubernetes.io/component: %s\n" -}}
{{-  .Release.Name | quote | printf "app.kubernetes.io/part-of: %s\n" -}}
{{-  "default" | quote | printf "network: %s" -}}
{{- end -}}

{{/*
Agent labels
*/}}
{{- define "netchecker.agentLabels" -}}
{{-  include "netchecker.chart" . | quote | printf "helm.sh/chart: %s\n" -}}
{{-  include "netchecker.agentSelectorLabels" . | printf "%s\n" -}}
{{-  .Chart.AppVersion | quote | printf "app.kubernetes.io/version: %s\n" -}}
{{-  .Release.Service | quote | printf "app.kubernetes.io/managed-by: %s" -}}
{{- end -}}

{{/*
Agent with host network selector labels
*/}}
{{- define "netchecker.hostnetAgentSelectorLabels" -}}
{{-  include "netchecker.selectorLabels" . | printf "%s\n" -}}
{{-  "agent" | quote | printf "app.kubernetes.io/component: %s\n" -}}
{{-  .Release.Name | quote | printf "app.kubernetes.io/part-of: %s\n" -}}
{{-  "hostnet" | quote | printf "network: %s" -}}
{{- end -}}

{{/*
Agent with host network labels
*/}}
{{- define "netchecker.hostnetAgentLabels" -}}
{{-  include "netchecker.chart" . | quote | printf "helm.sh/chart: %s\n" -}}
{{-  include "netchecker.hostnetAgentSelectorLabels" . | printf "%s\n" -}}
{{-  .Chart.AppVersion | quote | printf "app.kubernetes.io/version: %s\n" -}}
{{-  .Release.Service | quote | printf "app.kubernetes.io/managed-by: %s" -}}
{{- end -}}

{{/*
Server image
*/}}
{{- define "netchecker.serverImage" -}}
{{-  $repo := .Values.server.image.repository -}}
{{-  $defaultTag := .Chart.AppVersion | printf "v%s" -}}
{{-  $tag := default $defaultTag .Values.server.image.tag -}}
{{-  printf "%s:%s" $repo $tag -}}
{{- end -}}

{{/*
Agent image
*/}}
{{- define "netchecker.agentImage" -}}
{{-  $repo := .Values.agent.image.repository -}}
{{-  $defaultTag := .Chart.AppVersion | printf "v%s" -}}
{{-  $tag := default $defaultTag .Values.agent.image.tag -}}
{{-  printf "%s:%s" $repo $tag -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "netchecker.serviceAccountName" -}}
{{-   if .Values.serviceAccount.create -}}
{{-     default (include "netchecker.fullname" .) .Values.serviceAccount.name -}}
{{-   else -}}
{{-     default "default" .Values.serviceAccount.name -}}
{{-   end -}}
{{- end -}}

{{/*
Create the name of the pod security policy to use
*/}}
{{- define "netchecker.pspName" -}}
{{-   if .Values.psp.create -}}
{{-     default (include "netchecker.fullname" .) .Values.psp.name -}}
{{-   else -}}
{{-     default "privileged" .Values.psp.name -}}
{{-   end -}}
{{- end -}}

{{/*
Is etcd container enabled
*/}}
{{- define "netchecker.etcdEnabled" -}}
{{-   if .Values.server.etcdEndpoints -}}
{{-     if eq (len .Values.server.etcdEndpoints) 1 -}}
{{-       if eq (index .Values.server.etcdEndpoints 0) "http://127.0.0.1:2379" -}}
{{-         true | toString -}}
{{-       end -}}
{{-     end -}}
{{-   end -}}
{{- end -}}

{{/*
Agent container
*/}}
{{- define "netchecker.agentContainer" -}}
{{-   $serverName := include "netchecker.fullname" . -}}
{{-   $serverPort := .Values.server.service.port | int -}}
{{-   $serverEndpoint := printf "%s:%d" $serverName $serverPort -}}
name: agent
image: {{ include "netchecker.agentImage" . }}
imagePullPolicy: {{ .Values.agent.image.pullPolicy }}
args:
  - "-logtostderr"
  - {{ .Values.agent.logLevel | int | printf "-v=%d" | quote }}
  - {{ .Values.agent.reportInterval | int | printf "-reportinterval=%d" | quote }}
  - {{ printf "-serverendpoint=%s" $serverEndpoint | quote }}
  {{- range $url := .Values.agent.probeUrls }}
  - {{ printf "-probeurls=%s" $url | quote }}
  {{- end }}
env:
  - name: MY_POD_NAME
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: metadata.name
  - name: MY_NODE_NAME
    valueFrom:
      fieldRef:
        apiVersion: v1
        fieldPath: spec.nodeName
resources:
  {{- toYaml .Values.agent.resources | nindent 2 }}
securityContext:
  {{- toYaml .Values.agent.securityContext | nindent 2 }}
{{- end -}}

{{/*
Agent Pod common proeprties
*/}}
{{- define "netchecker.agentPodCommon" -}}
{{- $agent := .Values.agent -}}
containers:
  - {{- include "netchecker.agentContainer" . | nindent 4 }}
{{- with .Values.imagePullSecrets }}
imagePullSecrets:
  {{- toYaml . | nindent 2 }}
{{- end }}
automountServiceAccountToken: false
serviceAccountName: {{ include "netchecker.serviceAccountName" . }}
securityContext:
  {{- toYaml $agent.podSecurityContext | nindent 2 }}
{{- with $agent.nodeSelector }}
nodeSelector:
  {{- toYaml . | nindent 2 }}
{{- end }}
{{- with $agent.tolerations }}
tolerations:
  {{- toYaml . | nindent 2 }}
{{- end }}
enableServiceLinks: false
priorityClassName: {{ $agent.priorityClassName | toString }}
{{- end -}}
